class User < ApplicationRecord
  enum role: [:regular, :admin]
  enum status: [:inactivo, :activo]
  has_many :payments

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  after_initialize :set_default_role, :set_default_status, :if => :new_record?

  # accepts_nested_attributes_for :payments, reject_if: :all_blank

  def set_default_role
    self.role ||= :regular
  end

  def set_default_status
    self.status ||= :inactivo
  end

end
