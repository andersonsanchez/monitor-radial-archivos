class Datalist < ApplicationRecord
  # validates :list_date, :list, presence: true
  has_one_attached :list
  validate :correct_file_type

  private

  def correct_file_type
    if list.attached? && !list.content_type.in?(%w(text/plain))
      errors.add(:list, 'Seleccionar solo archivos con extensión .txt /')
    end
  end

end
