class Song < ApplicationRecord
  require 'csv'
  # validate :correct_file_type

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
      Song.create! row.to_hash
    end
  end

  private

  # def correct_file_type
  #   if file.attached? && !file.content_type.in?(%w(text/csv))
  #     errors.add(:file, 'Seleccionar solo archivos con extensión .csv /')
  #   end
  # end

end
