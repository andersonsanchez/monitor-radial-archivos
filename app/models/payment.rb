class Payment < ApplicationRecord
  belongs_to :user
  enum paid_out: [:pendiente, :procesado]
  validates :invoice, :paid_out, presence: true
  validate :correct_file_type
  has_one_attached :invoice

  # after_update :set_user_active
  accepts_nested_attributes_for :user, update_only: true

  private

    def correct_file_type
      if invoice.attached? && !invoice.content_type.in?(%w(image/jpg image/jpeg image/png application/pdf))
        errors.add(:invoice, 'Seleccionar solo archivos con extensión .jpg / .jpeg / .png / .pdf')
      end
    end

    # def set_user_active
    #   @user = User.find(params[:id])
    #   @user.status = 'activo'
    #   @user.save
    # end
end
