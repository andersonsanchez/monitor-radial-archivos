class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.notify.subject
  #
  def notify(user)
    @user = user
    mail(to: @user.email, subject: '¡Tu Suscripción ha sido Activada!')
  end

  def send_pay(user)
    @user = user
    mail(to: @user.email, subject: '¡Pago enviado satisfactoriamente!')
  end

  def confirmation(user)
    @user = user
    mail(to: 'admin@monitor-radial.com', subject: "¡Pago de #{@user.email} Recibido!")
  end

end
