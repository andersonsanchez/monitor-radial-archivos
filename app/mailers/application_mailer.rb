class ApplicationMailer < ActionMailer::Base
  default from: 'info@monitor-radial.com'
  layout 'mailer'
end
