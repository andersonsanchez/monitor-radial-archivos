class BroadcastersController < ApplicationController
  before_action :authenticate_user!, except: [:index]
  before_action :authenticate_admin, except: [:index]
  before_action :set_broadcaster, only: [:edit, :update, :destroy]
  layout :resolve_layout

  def manage
    @broadcasters = Broadcaster.all
  end

  def index
    @broadcasters = Broadcaster.all
  end

  def new
    @broadcaster = Broadcaster.new()
  end

  def create
    @broadcaster = Broadcaster.new(broadcaster_params)
    if @broadcaster.save
        redirect_to broadcasters_manage_path(), notice: '¡ Emisora Agregada Correctamente !'
    else
      render :new
    end  
  end

  def edit
    
  end

  def update
    if @broadcaster.update(broadcaster_params)
      redirect_to broadcasters_manage_path, notice: 'Emisora Modificada Correctamente.'
    else
      render :edit
    end
  end

  def destroy
    respond_to do |format|
      if @broadcaster.destroy
        format.html { redirect_to broadcasters_manage_path, notice: '¡ Emisora Eliminada Correctamente !'}
      else
        format.html { redirect_to broadcasters_manage_path, notice: '¡ No se pudo Eliminar la Emisora seleccionada !'}
      end
    end

  end

  private
  def authenticate_admin
    if current_user.role != 'admin'
      redirect_back fallback_location: root_path, alert: "No posee los permisos necesarios"
    end  
  end

  def broadcaster_params
    params.require(:broadcaster).permit(:name, :email, :phone, :ref, :location, :contact_name, :id)
  end

  def set_broadcaster
    @broadcaster = Broadcaster.find(params[:id])
  end

  def resolve_layout
    case action_name
    when "manage", "new"
      "admin"
    end
  end

end
