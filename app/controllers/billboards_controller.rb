class BillboardsController < ApplicationController
  before_action :authenticate_user!

  def index
    @user = current_user
  end

  def top_100

  end

  def top_latino

  end

  def top_tradicional

  end

  def top_balada

  end

  def top_salsa

  end

  def top_pop

  end

  def top_rock

  end

  def top_emisoras

  end

end
