class GenresController < ApplicationController
  before_action :set_genre, only: [:show, :edit, :update, :destroy]
  layout :resolve_layout
  def index
    @genres = Genre.all
  end

  def new
    @genre = Genre.new
  end

  def create
    @genre = Genre.new(genre_params)
    if @genre.save
      redirect_to genres_path, notice: '¡ Género Musical Agregado Correctamente !'
    else
      render :new
    end
  end

  def edit

  end

  def update
    if @genre.update(genre_params)
      redirect_to genres_path, notice: '¡ Género Musical Actualizado Correctamente !'
    else
      redirect_to genres_path, alert: '¡ No se pudo Actualizar el registro seleccionado !'
    end
  end

  def destroy
    if @genre.destroy
      redirect_to genres_path, notice: '¡ Género Musical Eliminado Correctamente !'
    else
      redirect_to genres_path, alert: '¡ No se pudo Elimina el registro seleccionado !'
    end
  end

  private

  def set_genre
    @genre = Genre.find(params[:id])
  end

  def genre_params
    params.require(:genre).permit(:name)
  end

  def resolve_layout
    case action_name
    when "index", "new"
      "admin"
    end
  end

end
