class PaymentsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_admin, except: [:new, :create]
  before_action :set_payment, only: [:edit, :update]
  layout :resolve_layout
def index
  @user = User.find(params[:user_id])
  @payments = Payment.all.where(user_id: @user).order(created_at: :desc)
end

def pendingpayments
  @payments = Payment.all.where(paid_out: 0)
end

def new
  @user = User.find(params[:user_id])
  @payment = Payment.new()
end

def create
  @user = User.find(params[:user_id])
  @payment = @user.payments.build(payment_params)
  respond_to do |format|
    if @payment.save
      UserMailer.send_pay(@user).deliver
      UserMailer.confirmation(@user).deliver
      format.html { redirect_to new_user_payment_path, notice: '¡ Recibo enviado correctamente, una vez confirmado el pago, recibirá una notificación por correo electrónico!' }
    else
      format.html { redirect_to new_user_payment_path, alert: '¡ Pago no enviado, el formato del archivo es incorrecto!'  }
    end
  end
end

def edit
  @user = User.find(params[:user_id])
end

def update
  @user = User.find(params[:user_id])
  if @payment.update(payment_params)
    UserMailer.notify(@user).deliver
    redirect_to user_payments_path(@user,@payments), notice: 'Registros procesados correctamente'
  else
    redirect_to payments_pending_path, alert: 'No se pudo procesar el pago seleccionado'
  end

  # render json: @payment
end

private

def authenticate_admin
  if current_user.role != 'admin'
    redirect_back fallback_location: root_path, alert: "No posee los permisos necesarios"
  end
end

def set_payment
  @payment = Payment.find(params[:id])
end

def payment_params
  params.require(:payment).permit(:paid_out, :invoice, user_attributes: [:status])
end

def user_params
  params.require(:user).permit(status:[])
end

def resolve_layout
  case action_name
  when "index", "pendingpayments", "edit"
    "admin"
  end
end

end
