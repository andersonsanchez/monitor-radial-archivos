class SongsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_admin
  layout :resolve_layout
  def new
  end

  def create
    Song.import(params[:file])
    redirect_to new_song_path, notice: "Lista de canciones agregadas correctamente!"
  end

  private
  def authenticate_admin
    if current_user.role != 'admin'
      redirect_back fallback_location: root_path, alert: "No posee los permisos necesarios"
    end
  end


  def resolve_layout
    case action_name
    when "new"
      "admin"
    end
  end
end
