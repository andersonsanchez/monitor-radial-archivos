Rails.application.routes.draw do
  resources :songs do
    collection { post :import}
  end
  get 'subscriptions', to: 'subscription#index'
  devise_for :users, :paths => 'users'
  resources :users do
    resources :payments
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'
  resources :broadcasters, except: [:show]
  resources :genres
  resources :datalists, only: [:new, :create]
  get 'payments/pending', to: 'payments#pendingpayments'
  # patch 'payments/pending', to: 'payments#update'
  get 'broadcasters/manage', to: 'broadcasters#manage'
  get 'administration/users', to: 'admins#manageusers'
  get 'administration', to: 'admins#index'
  get 'billboard', to: 'billboards#index'
  get 'billboard/top_100', to: 'billboards#top_100'
  get 'billboard/top_latino', to: 'billboards#top_latino'
  get 'billboard/top_tradicional', to: 'billboards#top_tradicional'
  get 'billboard/top_balada', to: 'billboards#top_balada'
  get 'billboard/top_salsa', to: 'billboards#top_salsa'
  get 'billboard/top_pop', to: 'billboards#top_pop'
  get 'billboard/top_rock', to: 'billboards#top_rock'
  get 'billboard/top_emisoras', to: 'billboards#top_emisoras'
  get 'contact', to: 'contact#index'
end
