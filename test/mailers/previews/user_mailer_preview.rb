# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/notify
  def notify
    UserMailer.notify User.new(email: 'test@monitor-radial.com')
  end

  def send_pay
    UserMailer.send_pay User.new(email: 'test@monitor-radial.com')
  end

  def confirmartion
    UserMailer.confirmation User.new(email: 'test@monitor-radial.com')
  end

end
