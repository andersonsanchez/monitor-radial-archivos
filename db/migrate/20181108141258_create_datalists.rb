class CreateDatalists < ActiveRecord::Migration[5.2]
  def change
    create_table :datalists do |t|
      t.date :list_date

      t.timestamps
    end
  end
end
