class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.date :pay_date
      t.integer :paid_out
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
